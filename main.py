'''
main.py 
'''

import sys
#sys.path.append('..')

#Import Common
from common.core import *
from common.audio import *
from common.writer import *
from common.mixer import *
from common.gfxutil import *
from common.wavegen import *
from common.buffers import *
import common.clock as c

#Import modules
from analog.analog import *
from midi.midi import *
from graphics.graphics import *
from playback.playback import *

#Import kivy graphics
from kivy.graphics.instructions import InstructionGroup
from kivy.graphics import Color, Ellipse, Rectangle, Line
from kivy.graphics import PushMatrix, PopMatrix, Translate, Scale, Rotate
import kivy.graphics.texture

#import uix
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.label import Label
from kivy.uix.behaviors import ToggleButtonBehavior
from kivy.uix.image import Image
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout

from kivy.properties import *

from kivy.lang import Builder
from kivy.app import App

from random import randint
import re

NUM_CHANNELS = 2
MODE = 0
UNDO = False
RECORDING = False
PLAY = False
TEXT = ''

Builder.load_string('''
<InputScreen>:
    size: (1600,1200)
    BeatsType:
        text: ('107,16')
        pos: (0,0)
<ModeButtons>:
    ModeType:
        state: 'down'
        source: 'graphics/mic_midi.png'
        pos: (1150,1050)
        idx: 0
    ModeType
        state: 'normal'
        source: 'graphics/mic_analog.png'
        pos: (1300,1050)
        idx: 1
    ModeType
        state: 'normal'
        source: 'graphics/midi.png'
        pos: (1450,1050)
        idx: 2
    RecType
        state: 'normal'
        source: 'graphics/record.png'
        pos: (200,1050)
    UndoType
        source: 'graphics/undo.png'
        pos: (350,1050)
    LogicType
        source: 'graphics/play.png'
        color: [.3,.8,.3,.8]
        pos: (50,1050)
''')

class UndoType(ToggleButtonBehavior, Image):
    active = BooleanProperty(False)
    allow_stretch = BooleanProperty(True)
    keep_ratio = BooleanProperty(False)
    def __init__(self, **kwargs):
        super(UndoType, self).__init__(**kwargs)
        self.allow_no_selection = True
        self.group = "undogroup"
        self.color=[1,1,1,.8]

        self.bind(pos=self.on_pos)
        self.release = True

    def on_press(self):
        global UNDO
        if self.release:
            self.release = False
            UNDO = True
            self.color = [.4,.4,.4,.8]
        else: UNDO = False

    def on_release(self):
        self.release = True
        self.color=[1,1,1,.8]

    def on_pos(self, widget, value):
        self.pos = value

class LogicType(ToggleButtonBehavior, Image):
    active = BooleanProperty(False)
    allow_stretch = BooleanProperty(True)
    keep_ratio = BooleanProperty(True)
    def __init__(self, **kwargs):
        super(LogicType, self).__init__(**kwargs)
        self.allow_no_selection = True
        self.group = "logicgroup"

        self.bind(state=self.on_state)
        self.bind(pos=self.on_pos)

    def on_state(self, widget, value):
        global PLAY
        if value == 'down':
            PLAY = True
            self.color = [1,1,1,.8]
            self.source = 'graphics/pause.png'
        else: #Normal
            PLAY = False
            self.color=[.3,.8,.3,.8]
            self.source = 'graphics/play.png'

    def on_pos(self, widget, value):
        self.pos = value

class RecType(ToggleButtonBehavior, Image):
    active = BooleanProperty(False)
    allow_stretch = BooleanProperty(True)
    keep_ratio = BooleanProperty(True)
    def __init__(self, **kwargs):
        super(RecType, self).__init__(**kwargs)
        self.allow_no_selection = True
        self.group = "recgroup"
        self.color=[1,1,1,.8]

        self.bind(state=self.on_state)
        self.bind(pos=self.on_pos)

    def on_state(self, widget, value):
        global RECORDING
        if value == 'down':
            self.color = [.7,.1,.1,.8]
            RECORDING = True
        else: #Normal
            self.color=[1,1,1,.8]
            RECORDING = False

    def on_pos(self, widget, value):
        self.pos = value

class ModeType(ToggleButtonBehavior, Image):
    active = BooleanProperty(False)
    allow_stretch = BooleanProperty(True)
    keep_ratio = BooleanProperty(True)
    #norm_image_size = BooleanProperty(True)
    def __init__(self, **kwargs):
        super(ModeType, self).__init__(**kwargs)
        self.allow_no_selection = False
        self.group = "mygroup"
        self.color=[1,1,1,.8]

        self.bind(state=self.on_state)
        self.bind(pos=self.on_pos)

    def on_state(self, widget, value):
        if value == 'down':
            self.color = [.4,.4,.4,.8]
            global MODE 
            MODE = self.idx
        else:
            self.color=[1,1,1,.8]

    def on_pos(self, widget, value):
        self.pos = value

class ModeButtons(FloatLayout):
    pass


class FloatInput(TextInput):
    pat = re.compile('[^0-9]')
    def insert_text(self, substring, from_undo=False):
        pat = self.pat
        if '.' in self.text:
            s = re.sub(pat, '', substring)
        else:
            s = '.'.join([re.sub(pat, '', s) for s in substring.split('.', 1)])
        return super(FloatInput, self).insert_text(s, from_undo=from_undo)

class BeatsType(ToggleButtonBehavior,TextInput):
    background_active = StringProperty('graphics/inputscreen.png')
    background_normal = StringProperty('graphics/midi_villain.png')
    background_color = ListProperty([1,1,1,1])
    font_size = NumericProperty(200)
    cursor_blink = BooleanProperty(True)
    multiline = BooleanProperty(False)
    #TextInput.text = AliasProperty('107,16')
    #cursor = AliasProperty((0,0))
    def __init__(self, **kwargs):
        super(BeatsType, self).__init__(**kwargs)
        print TextInput.text
        # self.bind(padding=self.on_padding)
        self.bind(pos=self.on_pos)
        self.bind(on_text_validate=self.on_enter)

    def on_enter(self, widget):
        global TEXT
        TEXT = self.text

    def on_pos(self, widget, value):
        self.pos = value

class InputScreen(FloatLayout):  # Changed to a BoxLayout for simplicity
    def __init__(self):
        super(InputScreen, self).__init__()
        self.orientation = "horizontal"
        self.kill = False
        self.pos = (500,500)

        #self.usr_input = TextInput( multiline=False) #focus=True, multiline=False)
        #self.add_widget(self.usr_input)

        def on_enter(self):
            self.kill = True
            print "IT ALSO PRESSED"
            #print 'user pressed enter'

        # Here we "bind" the callback to the TextInput's 'text' property
        #self.usr_input.bind(on_text_validate=on_enter)

    def get_kill(self):
        return self.kill

        # Called whenever the 'text' property of our TextInput is modified
        # def callback(instance, value):

    def get_text(self):
        return self.usr_input.text

class MainWidget(BaseWidget) :
    def __init__(self):
        super(MainWidget, self).__init__()

        self.pitch = PitchDetector()
        try:
            self.midi = MidiInput(0)
        except IOError as e:
            self.midi = None
        self.audio = Audio(NUM_CHANNELS, input_func=self.pitch.get_notes)
        self.mixer = Mixer()
        self.audio.set_generator(self.mixer)

        self.clock = c.Clock()
        self.tempo_map = c.SimpleTempoMap()
        self.sched = c.Scheduler(self.clock, self.tempo_map)

        self.clock.stop()
        self.started = False

        self.recording = False
        self.pressed = False

        self.graphics = Graphics(self.pitch, self.recording, self.midi)
        self.add_widget(self.graphics)

        self.sequencer = Sequencer(self.mixer, self.pitch, self.graphics, self.midi, self.sched)
        self.sequencer.play_toggle() #Pauses graphics

        self.toggle_on = True
        self.info = topleft_label()
        self.add_widget(self.info)

        self.mode = ModeButtons()
        self.add_widget(self.mode, index=0, canvas='before')
        self.inputscreen = InputScreen()
        self.add_widget(self.inputscreen)
        self.beats = 120

    def start(self):
        self.sequencer.play_toggle()
        self.started = True

    def on_key_down(self, keycode, modifiers):
        # toggle recording
        if keycode[1] == 'r':
            global RECORDING
            if self.recording: #stop recording
                self.sequencer.stop_recording()
            else: #start recording
                self.sequencer.start_recording()
            self.recording = self.sequencer.is_recording()
            RECORDING = self.recording
            
        elif keycode[1] == 'h':
            self.toggle_on = not(self.toggle_on)

        if self.started:
            inst = lookup(keycode[1], '12345', (0,1,2,3,4))
            if inst != None:  
                self.sequencer.set_instrument(inst)

    def on_update(self) :
        self.audio.on_update() # includes pitch detector update

        self.sequencer.mode = MODE
        global UNDO
        global RECORDING
        global PLAY
        global TEXT
        if TEXT and not self.started:
            self.started = True
            txt = TEXT.split(",")
            self.bpm = int(txt[0])
            self.beats = int(txt[1])
            self.remove_widget(self.inputscreen)
            self.sequencer.get_user_input(self.bpm, self.beats)
            self.start()
        if UNDO:
            UNDO = False
            self.sequencer.undo()

        if RECORDING:  #True
            self.sequencer.start_recording() 
        else: 
            self.sequencer.stop_recording()
        self.recording = RECORDING

        if PLAY and not self.pressed:
            self.pressed = True
            self.sequencer.play_toggle()
        elif not PLAY and self.pressed:
            self.pressed = False
            self.sequencer.play_toggle()

        if self.midi != None:
            self.midi.on_update()
        self.sequencer.on_update()
        # if self.sequencer.recording: self.graphics.recording = True
        # else: self.graphics.recording = False
        # #self.graphics.on_update()
        self.info.text = ''
        if self.toggle_on:
            self.info.text = '\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n'+'Num of Tracks: ' + str(len(self.sequencer.tracks)) + '\n'
            self.info.text += "1, 2, 3, 4, 5: Switch Instrument: %s\n" % ("E. Piano","Pan Flute","Clarinet","Marimba","Percussion")[self.sequencer.instrument]
<<<<<<< HEAD
            self.info.text += 'Loop Time Left: %.0f\n' % self.sequencer.loop_time_left() 
            self.info.text += 'Toggle Help Menu (H)' 
=======
            # self.info.text += 'Loop Time Left: %.0f\n' % self.sequencer.loop_time_left()
            self.info.text += 'Toggle Help Menu (H)'

>>>>>>> 74b5f3346f3716f8611baa648db383a526a1c948
run(MainWidget)
