#graphics.py
import sys
sys.path.append('..')
from common.core import *
from common.audio import *
from common.mixer import *
from common.wavegen import *
from common.wavesrc import *
from common.gfxutil import *
from analog.analog import *
# from playback.playback import FALLING_TIME

from kivy.graphics.instructions import InstructionGroup
from kivy.graphics import Color, Ellipse, Line, Rectangle, BorderImage
from kivy.graphics import PushMatrix, PopMatrix, Translate, Scale, Rotate
from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.image import Image
from kivy.core.image import Image
from kivy.clock import Clock as kivyClock


import random
import numpy as np
import bisect
import copy
LIST=[]

def set_falling_time(t):
    global FALLING_TIME
    FALLING_TIME = t


class Graphics(BaseWidget) : #For individual testing of components, should call Instruction Groups from main.py
    def __init__(self, pitch, recording, midi):
        super(Graphics, self).__init__()
        self.info = topleft_label()
        self.add_widget(self.info)
        
#        #Create MIDI Villain Brand
#        self.canvas.add(Color(1,1,1))
#        spec_xsize = Window.width
#        spec_ysize = Window.height
#        spec_xpos = Window.width/2 - spec_xsize/2
#        spec_ypos = Window.height/2 - spec_ysize/2 
#        self.canvas.add(Rectangle(pos=(spec_xpos, spec_ypos), size=(spec_xsize, spec_ysize), texture=Image('graphics/spectral.png').texture))
#        scale = 200
#        ratio = (3,1)
#        xsize = ratio[0]*scale
#        ysize = ratio[1]*scale
#        xpos = Window.width/2 - xsize/2
#        ypos = Window.height/2 - ysize/2 
#        self.canvas.add(Rectangle(pos=(xpos, ypos), size=(xsize, ysize), texture=Image('graphics/midi_villain.png').texture))
#        button = Button(text='Hello world', font_size=14)

        #Create Piano object
        self.piano = PianoDisplay()
        self.canvas.add(self.piano)
        self.yaxis = sorted(LIST)
        self.in_range = self.piano.in_range

        #Audio setup
        NUM_CHANNELS = 2
        self.pitch = pitch

        self.recording = recording
        self.channel_select = 0
        self.input_buffers = []
        self.live_wave = None

        self.mode = 0

        self.midi = midi
        self.play = True

        #Record objects
        self.recorded = AnimGroup()
        self.canvas.add(self.recorded)

        #Playback objects
        self.objects = AnimGroup()
        self.canvas.add(self.objects)
        self.falling_notes = []

    def on_key_down(self, keycode, modifiers):
        pass

    def add_line(self, thick=False):
        bar = BarDisplay(self.piano.height, thick)
        self.objects.add(bar)
        # print "bar type: " + str(type(bar))

    def add_falling_note(self, pitch, duration, color, trackid):
        # note = FallingNote(pitch, (0, Window.height), Window.height, self.piano.in_range, Window.height, self.piano.height)
        note = KFFallingNote(pitch, duration, self.yaxis, self.in_range, color, trackid)
        self.falling_notes.append(note)
        self.objects.add(note)
        return note # for sequencer to keep so I can turn off that note

    def remove_falling_notes(self, trackid):
        # note.contin = False
        #print "trackid to remove: " + str(trackid)
        for note in self.objects.get_objects():
            # print "note " + str(note)
            if 'BarDisplay' not in str(type(note)):
                #print "trackid of cur note: " + str(note.get_trackid())
                if note.get_trackid() == trackid:
                    # self.objects.get_objects().remove(note)
                    note.kill()

        # self.falling_notes.remove(note)

    def on_key_up(self, keycode):
        pass

    def set_mode(self,mode):
        self.mode = mode

    def set_play(self, play):
        self.play = play

    def on_update(self) :
        if self.mode != 2:
            if self.pitch.get_status() == "on" and self.recording: #Falling = (0, Window.height) Rising = (0, Window.width+ self.piano.height)
                recordnote = RecNote(self.pitch, self.pitch.get_pitch(), (0, Window.height/2), Window.width, self.piano.in_range, Window.height, self.piano.height, self.yaxis)
                self.recorded.add(recordnote)
            for recnote in self.recorded.objects:
                recnote.set_recording(self.recording)
        else: 
            data = self.midi.get_statuses()
            # print "data: ",data
            for i in range(len(data)): #array of 32 statuses
                if data[i] == "on" and self.recording:
                    pitch = i
                    recordnote = RecNote(self.midi, pitch+53, (0, Window.height/2), Window.width, self.piano.in_range, Window.height, self.piano.height, self.yaxis)
                    self.recorded.add(recordnote)
                for recnote in self.recorded.objects:
                    recnote.set_recording(data[i])

        #for all recorded objects
        if self.play:
            self.recorded.on_update()
            self.objects.on_update()
        self.info.text =''
        #self.info.text = '\n\n\n\n\n\n'
        #self.info.text += 'Recorded: '+str(len(self.recorded.objects))+ '   Bar/Plackback: '+ str(len(self.objects.objects))


class BlackKeyDisplay(InstructionGroup):
    def __init__(self, pos, key, numkeys, height):
        super(BlackKeyDisplay, self).__init__()
        self.color = Color(.2,.2,.2)
        self.key  = key
        self.numkeys = numkeys
        self.height = height
        self.pos = pos

        self.octave = self.key/5 #5 because there's 5 black keys in an octave
        num_octaves = float(numkeys)/7
        octave_width = Window.width/num_octaves
        halfkey_width = Window.width/(4.*self.numkeys)
        self.key_width = Window.width/(self.numkeys*2.)
        if self.key % 5 == 0: #It's an A#/Bb
            self.offset = octave_width*(1./7) - halfkey_width
        elif self.key % 5 == 1: #It's a C#/Db
            self.offset = octave_width*(3./7) - halfkey_width
        elif self.key % 5 == 2: #It's an D#/Eb
            self.offset = octave_width*(4./7) - halfkey_width
        elif self.key % 5 == 3: #It's an F#/Gb
            self.offset = octave_width*(6./7) - halfkey_width
        elif self.key % 5 == 4: #It's an G#/Ab
            self.offset = octave_width - halfkey_width
        bottom = self.height/3
        top = self.height - bottom
        self.xpos = self.octave*octave_width + self.offset
        LIST.append(self.xpos+ halfkey_width)
        self.rect = Rectangle(pos=(self.xpos,bottom+self.pos[1]), size=(self.key_width,top))
        self.add(self.color)
        self.add(self.rect)
        
    # Useful if key is flowing in/out of the black key
    def on_update(self, dt):
        pass

# Display for a single white key on the piano
class WhiteKeyDisplay(InstructionGroup):
    def __init__(self, pos, key, numkeys, height):
        super(WhiteKeyDisplay, self).__init__()
        self.color = Color(1,1,1)
        self.pos = pos
        self.key  = key
        self.numkeys = numkeys
        self.key_width = Window.width/(self.numkeys)
        self.xpos = float(self.key)/self.numkeys*Window.width
        LIST.append(self.xpos+float(self.key_width/2))
        self.height = height
        self.rect = Rectangle(pos=(self.xpos,self.pos[1]), size=(self.key_width,self.height))
        self.add(self.color)
        self.add(self.rect)
        
    # Usefel if white key has flow in/out
    def on_update(self, dt):
        pass

# Display for base behind the piano
class BaseDisplay(InstructionGroup):
    def __init__(self,pos, height):
        super(BaseDisplay, self).__init__()
        self.colors = [Color(.1,.7,.2),Color(.75,.25,.25)] #.75,.25,.25
        self.rec = False
        self.height = height
        self.nowbar = 10
        self.pos = pos
        self.rect = Rectangle(pos = self.pos, size = (Window.width, self.height+self.nowbar))
        self.color = self.colors[self.rec]
        self.add(self.color)
        self.add(self.rect)
        
    def set_color(self, rec):
        self.rec = rec
        self.color = self.colors[self.rec]
        self.add(self.color)
        self.add(self.rect)

    # Usefel if white key has flow in/out
    def on_update(self, dt):
        pass

# Displays and controls all Graphical Elements: Black Keys/White Keys/Falling notes/Rising notes
class PianoDisplay(InstructionGroup):
    def __init__(self, num_keys = 88):
        super(PianoDisplay, self).__init__() 
        #Tuning parameters 
        self.num_keys = num_keys
        self.height = Window.height/7
        self.in_range = (21,109) #Midi from lowest A to high C
        
        #Shifting of piano  
        self.pos = (0, Window.height/2 - self.height)

        #Set base board 
        self.base = BaseDisplay(self.pos, self.height)
        self.add(self.base)


        #Lowest a (on piano) = 21 in MIDI 
        whitekeys = set([0,2,3,5,7,8,10])
        blackkeys = set([1,4,6,9,11])

        #Get spacing for black/white keys
        num_black_keys = 0 #52
        num_white_keys = 0 #36
        for key in range(self.num_keys):
            keytype = key%12
            if keytype in whitekeys:
                num_white_keys += 1
            else:
                num_black_keys += 1
        
        #Place white keys
        for key in range(num_white_keys):
            graphic = WhiteKeyDisplay(self.pos, key, num_white_keys, self.height)
            self.add(graphic)

        #Place black keys over white keys
        for key in range(num_black_keys):
            graphic = BlackKeyDisplay(self.pos, key, num_white_keys, self.height)
            self.add(graphic)

    # Call every frame to make Keys flow in/out of the piano on playback/recording
    def on_update(self, dt) :
        pass

#(self, pos, height, num_pts, in_range, color):

# display for a single barline
class BarDisplay(InstructionGroup):
    def __init__(self, piano_height, thick):
        super(BarDisplay, self).__init__()
        self.thick = thick
        if self.thick:
            w = 3
            self.color = Color(.7,.7,.7)
        else:
            w = 1
            self.color = Color(.5,.5,.5)
        self.add(self.color)
        self.line = Line(points=[0,Window.height,Window.width,Window.height], width = w)
        self.add(self.line)
        self.piano_height = piano_height
        self.nowbar = Window.height/2
        self.time = 0
        self.bar_anim = KFAnim((0,self.nowbar),(FALLING_TIME/2, 0), (FALLING_TIME/2+.0000001, Window.height),(FALLING_TIME,self.nowbar)) # Get falling note off screen?
        self.on_update(0)
    
    def set_y(self, y_pos):
        self.line.points = [0, y_pos, Window.width, y_pos]

    def on_update(self, dt):
        self.time += dt
        self.set_y(self.bar_anim.eval(self.time))
        return self.bar_anim.is_active(self.time)


##################### Laura's experimental area haha

class KFFallingNote(InstructionGroup):
    def __init__(self, pitch, duration, yaxis, in_range, color, trackid):
        super(KFFallingNote, self).__init__()
        self.color = Color(*color)
        self.add(self.color)
        self.yaxis = yaxis
        self.in_range = in_range
        self.nowbar = Window.height/2
        self.duration = duration
        self.cur_pitch = pitch+12 #Octave error
        self.trackid = trackid
        # self.piano_height = piano_height

        self.y_length = ( Window.height / FALLING_TIME ) * self.duration # I imported FALLING_TIME (which is 1 whole loop length) from playback.py
        self.x = int( np.interp( self.cur_pitch, range(self.in_range[0],self.in_range[1]), self.yaxis ))

        self.line = Line(points=[self.x, Window.height+self.y_length, self.x, Window.height], width = 6, cap = 'none') # width needs to be piano key width?
        self.line2 = Line(points=[self.x, Window.height+self.y_length, self.x, Window.height], width = 6, cap = 'none') # width needs to be piano key width?
        self.add(self.line)
        self.add(self.line2)

        self.y_anim = KFAnim((0,Window.height),(FALLING_TIME/2, Window.height),(FALLING_TIME,self.nowbar)) #Top to mid
        self.y_anim2 = KFAnim((0,self.nowbar),(FALLING_TIME/2 + duration, -self.y_length), (FALLING_TIME-.0000001, -self.y_length),(FALLING_TIME,self.nowbar)) #Mid to bottom
        self.time = 0

        self.keep = True

        self.on_update(0)

    def kill(self):
        self.keep = False

    def get_trackid(self):
        return self.trackid

    def set_y(self, y_pos, line):
        # y_pos is the bottom of the note, closest to piano nowbar
        line.points = [ self.x, y_pos+self.y_length, self.x, y_pos ] 

    def on_update(self, dt):
        self.time += dt
        if self.keep:
            self.set_y(self.y_anim.eval(self.time), self.line)
            self.set_y(self.y_anim2.eval(self.time), self.line2)
            return self.y_anim.is_active(self.time)
        else: return self.keep        

############################################################
class FallingNote(InstructionGroup):
    def __init__(self, pitch, pos, height, in_range, num_pts, piano_height):
        super(FallingNote, self).__init__()
        self.color = Color(.1,.7,.2)

        self.velocity = 2
        self.num_pts = (num_pts - piano_height)/2
        self.range = in_range
        self.height = height
        self.yaxis = sorted(LIST)
        self.cur_pitch = pitch+12 #Octave error
        y = int( np.interp( self.cur_pitch, range(self.range[0],self.range[1]), self.yaxis ))
        self.points = np.zeros(4, dtype = np.int) 
        self.points[1::2] = np.arange(len(self.points)/2) * self.velocity 
        self.points = self.points[::-1] # To reverse
        self.points[-1] = y
        self.points[1] = y
        self.contin = True #Continue adding points to lines?

        self.idx = 0
        self.line = Line(width = 6)
        self.line.points = self.points
        self.add(PushMatrix())
        self.add(Translate(*pos)) 
        self.add(Rotate(angle = -90)) #Angle
        self.add(self.color)
        self.add(self.line)
        self.add(PopMatrix())

        self.add(self.anim_group)
        
        self.x = self.points[0]

        self.is_active = True

    def add_point(self, y):
        if self.contin: #Extrude line
            self.cur_pitch = y
            y = int( np.interp( y, range(self.range[0],self.range[1]), self.yaxis ))
            if len(self.points) < self.num_pts: #Make line bigger until out of screen
                self.x += self.velocity
                self.points = np.insert(self.points, 0, [self.x,y])
            else: 
                self.points[3:len(self.points):2] = self.points[1:len(self.points)-2:2]
                self.points[1] = y
        elif not self.contin and self.points.any(): #Shift things over until out of view and delete them
            self.points[2:len(self.points):2] = self.points[:len(self.points)-2:2] #shift things over
            self.points = np.delete(self.points, 0)
            self.points = np.delete(self.points, 0)
        self.line.points = self.points
        if not len(self.line.points):
            self.is_active = False
            # no points in line, remove from list (self.falling_notes)

    def on_update(self): #, status, pitch):
        self.add_point(self.cur_pitch)
        self.anim_group.on_update()
        # print status
        # if status == "holdoff" or status == "off":
        #     self.contin = False #Can only ever be set to false
        # elif status == "change":
        #     print "CHANGE:",pitch
        #     self.add_point(pitch+12)
        # elif status == "holdon":
        #     self.add_point(self.cur_pitch)

class RecNote(InstructionGroup):
    def __init__(self, detect, pitch, pos, height, in_range, num_pts, piano_height, yaxis):
        super(RecNote, self).__init__()
        self.color = Color(.75,.25,.25)
        self.detect = detect
        self.recording = True

        self.num_pts = (num_pts - piano_height)/2
        self.velocity = Window.height/FALLING_TIME
        self.pos2 = (pos[0], pos[1]+Window.height)
        self.range = in_range
        self.height = height
        self.yaxis = yaxis
        self.cur_pitch = pitch+12 #Octave error
        self.pitch = pitch
        y = int( np.interp( self.cur_pitch, range(self.range[0],self.range[1]), self.yaxis ))
        self.points = np.array([0,y], dtype = np.float) 
        self.contin = True #Continue adding points to lines?

        self.idx = 0
        self.line = Line(width = 6, cap = 'none')
        self.line2 = Line(width = 6, cap = 'none')
        self.line.points = self.points
        self.line2.points = self.points
        #Line 1
        self.add(PushMatrix())
        self.add(Translate(*pos)) 
        self.add(Rotate(angle = -90))
        self.add(self.color)
        self.add(self.line)
        self.add(PopMatrix())
        #Line 2
        self.add(PushMatrix())
        self.add(Translate(*self.pos2)) 
        self.add(Rotate(angle = -90))
        self.add(self.color)
        self.add(self.line2)
        self.add(PopMatrix())

        self.is_active = True
        self.stop = False

        self.time = 0
        self.on_update(0)

    def add_point(self, y, dt): #[1600, pitch, 1596, pitch .....]
        self.cur_pitch = y
        y = int( np.interp( y, range(self.range[0],self.range[1]), self.yaxis ))
        if len(self.points) < self.num_pts: #Make line bigger until out of screen
            self.points[0::2] += self.velocity*dt
            self.points = np.insert(self.points, len(self.points), [0,y])
        else: #Line is still growing so shift right
            self.points[0:len(self.points):2] += self.velocity*dt #shift things over
            #self.points[3:len(self.points):2] = self.points[1:len(self.points)-2:2]
            #self.points[1] = y
        self.line.points = self.points
        self.line2.points = self.points

    def shift_point(self, dt):
        self.points[0:len(self.points):2] += self.velocity*dt #shift things over
        if self.points.any():
            for i in range(len(self.points)/2):
                if self.points[0] > Window.height - 5: #If line point is past nowbar, delete 
                    self.points = np.delete(self.points, [0,1])
                else: break
        else:
            self.is_active = False
        self.line.points = self.points
        self.line2.points = self.points

    def set_recording(self, recording):
        self.recording = recording

    def on_update(self, dt):
        self.time += dt

        if "PitchDetector" in str(type(self.detect)):
            pitch = self.detect.get_pitch()
            status = self.detect.get_status()
            #print "Time:",self.time,"\tPitch:",pitch,"\tStatus:",status
            if self.recording:
                if status == "holdoff" or status == "off":
                    self.stop = True
                    self.shift_point(dt)
                elif status == "change" and not(self.stop):
                    self.add_point(pitch+12, dt)
                elif (status == "holdon" or status == "on")  and not(self.stop):
                    self.add_point(self.cur_pitch, dt)
                elif self.stop:
                    self.shift_point(dt)
            else:
                self.shift_point(dt)
        else: 
            status = self.detect.get_statuses()[self.pitch-53]
            pitch = self.pitch
            
            #print "Time:",self.time,"\tPitch:",pitch,"\tStatus:",status
            if self.recording:
                if status == "holdoff" or status == "off":
                    self.stop = True
                    self.shift_point(dt)
                elif status == "change" and not(self.stop):
                    self.add_point(pitch+12, dt)
                elif (status == "holdon" or status == "on")  and not(self.stop):
                    self.add_point(self.cur_pitch, dt)
                elif self.stop:
                    self.shift_point(dt)
            else:
                self.shift_point(dt)

        return self.is_active

run(Graphics)
