'''
analog.py, Galen
take analog mic input, convert to MIDI, send to graphics and sequencer, save wav files

TODO:
-postprocess self.MIDI_data to combine notes that are close together
    -in general can do a lot of post processing. align/quantize timings? (would depend on sequencer data)
-big jumps are still not working well, try other types of smoothing
    -alpha?
    -running median within each note?
    -mode within each note?
    -different way of averaging rather than nonzero (maybe this is causing the jumps not to work?)
    -ask sarah?
-rhythms don't really work

NEED TO MAKE RED NOTES LOOK BETTER??

for post processing:
-IDEA: save raw data to post process. if many changes detected in one region, get pitch before changes and then after

timer cases:
on -> holdon -> off
on -> holdon -> change
change -> holdon -> change
change -> holdon -> off

'''

import sys
sys.path.append('..')

from common.core import *
from common.audio import *
from common.writer import *
from common.mixer import *  
from common.gfxutil import *
from common.wavegen import *
from common.buffers import *

from random import randint
import numpy as np
import aubio
# import audioop

NUM_CHANNELS = 2

'''
PitchDetector object gets pitch and sends data to graphics, sequencer

'''
class PitchDetector(object):
    def __init__(self):
        super(PitchDetector, self).__init__()

        #clear log file
        # open('log.txt', 'w').close()

        # number of frames to present to the pitch detector each time
        self.buffer_size = 1024

        # confidence threshold
        self.conf_thresh = .8

        # set up the pitch detector
        #try setting 2048 to higher?
        self.pitch_o = aubio.pitch("default", 2048, self.buffer_size, Audio.sample_rate)
        self.pitch_o.set_tolerance(self.conf_thresh)
        self.pitch_o.set_unit("midi")

        # buffer allows for always delivering a fixed buffer size to the pitch detector
        self.buffer = FIFOBuffer(self.buffer_size * 8, buf_type=np.float32)

        self.cur_pitch = 0.0
        self.prev_pitch = 0
        self.cur_volume = 0 #in db

        self.volume_avg = 0
        self.volume_hist_count = 0

        self.pitch_avg = 0
        self.pitch_hist_count=0

        self.cur_db = -100

        self.volume_thresh = 15

        self.cur_status = "holdoff"
        self.prev_status = "off"

        #frames to count until set status to "on
        self.on_hold_count = -1

        self.prev_on_time = 0
        self.cur_on_time = 0
        self.on_pitch = 0

        self.nonzero_hist = []
        self.nonzero_vol = []
        self.nonzero_hist_size = 5
        self.nonzero_vol_size = 5

        self.pitch_for_graphics = 0
        
        self.MIDI_data = []

        self.input_buffers = []

        self.filename = None

        self.recording = False

        self.hysteresis = .45
        self.alpha = .5
        # self.avg_window = 10
        self.history = np.array([0])

        self.recording_start = kivyClock.get_time()
        self.last_on_time = self.recording_start

        self.duration = self.recording_start
        self.duration_thresh = .1

    #TODO: make a list of n pitches to avg over
    #make smoothing pitches avg n of previous excluding 0s?
    #when two pitches are close together and have short lengths, merge
    def get_notes(self, signal, num_channels):
        self.prev_pitch = self.cur_pitch
        self.prev_status = self.cur_status

        # self.on_hold_count -=1

        self.buffer.write(signal) # insert data
        #add to buffer if recording
        if self.recording:
            self.input_buffers.append(signal) 

        conf = 0
        pitch = 0

        #get volume
        if num_channels == 2: mono = signal[0::2] # pick left or right channel
        else: mono = signal
        rms = np.sqrt(np.mean(mono ** 2))
        rms = np.clip(rms, 1e-10, 1) # don't want log(0)
        db = 20 * np.log10(rms)
        vol = 128/10**(db/((-10.) * (-70/-21.07))) - 1
        if vol > 127: vol = 127
        self.cur_volume = vol

        #do avg volume over note
        self.volume_avg = (self.volume_avg*self.volume_hist_count+self.cur_volume)/(self.volume_hist_count+1)

        # if len(self.nonzero_vol) > 0:
        #     self.cur_volume = np.mean(self.nonzero_vol)
        # if len(self.nonzero_vol) >= self.nonzero_vol_size: 
        #     self.nonzero_vol = self.nonzero_vol[1:]
        # self.nonzero_vol = np.append(self.nonzero_vol,vol)


        # read data in the fixed chunk sizes, as many as possible.
        # keep only the highest confidence estimate of the pitches found.
        pitch = 0
        zero_count = 0
        while self.buffer.get_read_available() > self.buffer_size:
            p, c = self._process_window(self.buffer.read(self.buffer_size))


            # if p > 65: self.conf_thresh = .9
            if p > 55: self.conf_thresh = 0.8
            elif p > 40: self.conf_thresh = 0.5
            else: self.conf_thresh = 0.35
            # self.conf_thresh = np.interp(p,np.arange(30,60,3),np.arange(.2,.8,.06))
            # print "conf_thresh: " + str(self.conf_thresh)

            if c > conf and c > self.conf_thresh: 
                pitch = p
            else: 
                # print "pitch not recognized"
                if self.volume_avg < self.volume_thresh: 
                    pitch = 0
                else: 
                    pitch = self.prev_pitch

        #pitch for graphics - no smoothing
        self.pitch_for_graphics = pitch

        # pitch set
        if len(self.nonzero_hist) > 0:
            self.cur_pitch = np.mean(self.nonzero_hist)

        #try to take nonzero window avg
        if pitch != 0:
            if len(self.nonzero_hist) >= self.nonzero_hist_size: self.nonzero_hist = self.nonzero_hist[1:] 
            self.nonzero_hist = np.append(self.nonzero_hist,pitch)
        else: self.cur_pitch = 0

        # trying nonzero window, doesn't seem to work
        # self.pitch_avg = (self.pitch_avg*self.pitch_hist_count+pitch)/(self.pitch_hist_count+1)
        # self.cur_pitch = self.pitch_avg

        #detecting events
        #hold count exceeded
        # if self.on_hold_count == 0:
        #     self.on_hold_count = 3
        #     self.cur_status = "on"

        #on
        if self.prev_pitch == 0 and self.cur_pitch > 0:
            self.cur_status = "on"
            # self.on_hold_count = 3

        #off
        elif self.prev_pitch > 0 and self.cur_pitch == 0:
            self.cur_status = "off"

        #holdoff
        elif self.prev_pitch == 0 and self.cur_pitch == 0:
            self.cur_status = "holdoff"

        #holdon
        elif self.prev_pitch > 0 and self.cur_pitch > 0 and abs(self.cur_pitch-self.prev_pitch) < self.hysteresis:
            self.cur_status = "holdon"
            self.cur_pitch = self.prev_pitch

        #change
        elif abs(self.cur_pitch-self.prev_pitch) > self.hysteresis:
            self.cur_status = "change"

        if self.cur_status == "on": 
            self.last_on_time = kivyClock.get_time()
            self.volume_avg = 0
            self.volume_hist_count = 0
            # self.pitch_avg = 0
            # self.pitch_hist_count = 0

        #if change, stop timer, save, then start again
        elif (self.cur_status == "change" and (self.prev_status == "change" or self.prev_status == "holdon")) or self.cur_status == "off":
            self.duration = kivyClock.get_time()-self.last_on_time #get duration
            #save
            # print "self.duration: " + str(self.duration)
            # print "self.cur_pitch: " + str(self.cur_pitch)
            # print "volume: " + str(self.cur_volume)

            #if not "off", current pitch should not be 0 (change)
            if self.cur_status != "off":
                # print "cur volume: " + str(self.cur_volume)
                if self.duration > self.duration_thresh and self.cur_pitch != 0 and self.volume_avg > self.volume_thresh:
                    if self.recording:
                        # print "appending: duration is " + str(self.duration) + ", volume is " + str(self.volume_avg) + ", pitch is " + str(self.cur_pitch)
                        # with open("log.txt", "a") as f:
                        #     f.write("appending: duration is " + str(self.duration) + ", volume is " + str(self.volume_avg) + ", pitch is " + str(self.cur_pitch) + "\n")
                        self.input_buffers.append(signal) 
                        self.MIDI_data.append((self.last_on_time-self.recording_start, self.duration, self.prev_pitch, self.volume_avg))
                # else: 
                    # print "not appending: duration is " + str(self.duration) + ", volume is " + str(self.volume_avg) + ", pitch is " + str(self.cur_pitch)
                    # with open("log.txt", "a") as f:
                    #         f.write("not appending: duration is " + str(self.duration) + ", volume is " + str(self.volume_avg) + "\n")
                self.last_on_time = kivyClock.get_time() #reset timer
                self.volume_avg = 0
                self.volume_hist_count = 0
                #if doing running pitch avg/median,etc., do here
                # self.pitch_avg = 0
                # self.pitch_hist_count = 0

            #if "off", current pitch will be 0, so append previous pitch
            if self.cur_status == "off": 
                if self.duration > self.duration_thresh: # and self.cur_volume > self.volume_thresh:
                    # print "cur volume: " + str(self.cur_volume)
                    if self.recording:
                        # print "appending: duration is " + str(self.duration) + ", volume is " + str(self.volume_avg)
                        # with open("log.txt", "a") as f:
                        #     f.write('appending\n')
                        self.input_buffers.append(signal) 
                        self.MIDI_data.append((self.last_on_time-self.recording_start, self.duration, self.prev_pitch, self.volume_avg))

            # if self.cur_status != "off": #only reset if change, not if off
            #     self.last_on_time = kivyClock.get_time() #reset timer

        # print self.cur_status,self.cur_pitch
        # with open("log.txt", "a") as f:
        #     f.write(str(self.cur_status) + ", " + str(self.cur_pitch) + '\n')
        return self.cur_status, self.cur_pitch

    # FOR SEQUENCER: after done recording, send current 
    def get_MIDI_data(self):
        if self.recording: print "Currently recording, stop recording to get MIDI data."
        else: 
            data = self.MIDI_data
            res = []
            for i in range(len(data)):
                # print i
                
                cur_start = data[i][0]
                cur_dur = data[i][1]
                cur_pitch = data[i][2]
                cur_vol = data[i][3]

                if i != 0:
                    prev_start = res[-1][0]
                    prev_dur = res[-1][1]
                    prev_pitch = res[-1][2]
                    prev_vol = res[-1][3] 

                    #check last elt in res with current element in data, merge if needed
                    # if abs(cur_pitch - prev_pitch) <= 1 and cur_start-(prev_start+prev_dur) < .02:
                    #     new_dur = prev_dur+cur_dur+(cur_start-prev_dur)-prev_start
                    #     res[-1] = (prev_start,new_dur,int(round(prev_pitch)),(cur_dur*cur_vol + prev_dur*prev_vol)/new_dur)
                        # print "appending at index : " + str(i)
                        # with open("log.txt", "a") as f:
                        #     f.write("appending at index : " + str(i) + "\n")
                    # if pitches are. one off?
                    if abs(cur_pitch-prev_pitch) <= 1 and cur_start-(prev_start+prev_dur) < .02:
                        new_dur = prev_dur+cur_dur+(cur_start-prev_dur)-prev_start
                        res[-1] = (prev_start,new_dur,int(round((cur_dur*cur_pitch + prev_dur*prev_pitch)/new_dur)),(cur_dur*cur_vol + prev_dur*prev_vol)/new_dur)
                        # print "diff, appending at index : " + str(i)
                    else: #otherwise append current note to res
                        res.append((data[i][0],data[i][1],int(round(data[i][2])),data[i][3]))
                else: res.append((data[i][0],data[i][1],int(round(data[i][2])),data[i][3])) #first elt

            # print "before: " + str(data)

            # with open("log.txt", "a") as f:
            #     f.write("before: " + str(data) + '\n')
                # f.write("after: " + str(res) + '\n')
            return res

            # return self.MIDI_data

    def write_MIDI_data(self):
        with open("log.txt", "a") as f:
            f.write(str(self.MIDI_data) + "\n")

    # FOR SEQUENCER: need to call this when starting to record
    #TODO: argument is file name, if None then don't save wav file
    def start_recording(self,filename=None):
        if filename: self.filename = filename 
        self.recording = True
        self.MIDI_data = []
        self.recording_start = kivyClock.get_time()

    # FOR SEQUENCER: need to call this after done recording
    #TODO save file when stopped
    def stop_recording(self):
        self.recording = False
        if self.filename: self.process_input()
        self.filename = None

    def process_input(self) :
        data = combine_buffers(self.input_buffers)
        # print 'live buffer size:', len(data) / NUM_CHANNELS, 'frames'
        write_wave_file(data, NUM_CHANNELS, self.filename)
        self.live_wave = WaveArray(data, NUM_CHANNELS)
        self.input_buffers = []

    def get_pitch(self):
        return self.pitch_for_graphics

#    def get_db(self):
 #       return self.cur_db

    def get_status(self):
        return self.cur_status

    def get_volume(self):
        return self.volume_avg

    # helper function for finding the pitch of the fixed buffer signal.
    def _process_window(self, signal):
        pitch = self.pitch_o(signal)[0]
        conf = self.pitch_o.get_confidence()
        return pitch, conf

    # #helper function to convert db to midi
    # #TODO: adjust this range to be smaller
    # def _db_to_midi(self, db):
    #     midi = ((db + 50) * 127) / 50
    #     if midi > 127: midi = 127
    #     if midi < 0: midi = 0
    #     return midi

# Same as WaveSource interface, but is given audio data explicity.
#TODO move this into audio.py? bc need this for both analog and midi modules
class WaveArray(object):
    def __init__(self, np_array, num_channels):
        super(WaveArray, self).__init__()
        self.data = np_array
        self.num_channels = num_channels

    # start and end args are in units of frames,
    # so take into account num_channels when accessing sample data
    def get_frames(self, start_frame, end_frame) :
        start_sample = start_frame * self.num_channels
        end_sample = end_frame * self.num_channels
        return self.data[start_sample : end_sample]

    def get_num_channels(self):
        return self.num_channels
