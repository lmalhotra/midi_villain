'''
main.py, Galen
for testing analog.py stuff
'''

import sys
sys.path.append('..')

from common.core import *
from common.audio import *
from common.writer import *
from common.mixer import *
from common.gfxutil import *
from common.wavegen import *
from common.buffers import *
from analog import *

from kivy.graphics.instructions import InstructionGroup
from kivy.graphics import Color, Ellipse, Rectangle, Line
from kivy.graphics import PushMatrix, PopMatrix, Translate, Scale, Rotate

from random import randint
import aubio

NUM_CHANNELS = 2

# graphical display of a meter
class MeterDisplay(InstructionGroup):
    def __init__(self, pos, height, in_range, color):
        super(MeterDisplay, self).__init__()
        
        self.max_height = height
        self.range = in_range

        # dynamic rectangle for level display
        self.rect = Rectangle(pos=(1,1), size=(50,self.max_height))

        self.add(PushMatrix())
        self.add(Translate(*pos))

        # border
        w = 52
        h = self.max_height+2
        self.add(Color(1,1,1))
        self.add(Line(points=(0,0, 0,h, w,h, w,0, 0,0), width=2))

        # meter
        self.add(Color(*color))
        self.add(self.rect)

        self.add(PopMatrix())

    def set(self, level):
        h = np.interp(level, self.range, (0, self.max_height))
        self.rect.size = (50, h)


# continuous plotting and scrolling line
class GraphDisplay(InstructionGroup):
    def __init__(self, pos, height, num_pts, in_range, color):
        super(GraphDisplay, self).__init__()

        self.num_pts = num_pts
        self.range = in_range
        self.height = height
        self.points = np.zeros(num_pts*2, dtype = np.int)
        self.points[::2] = np.arange(num_pts) * 2
        self.idx = 0
        self.mode = 'scroll'
        self.line = Line( width = 1 )
        self.add(PushMatrix())
        self.add(Translate(*pos))
        self.add(Color(*color))
        self.add(self.line)
        self.add(PopMatrix())

    def add_point(self, y):
        y = int( np.interp( y, self.range, (0, self.height) ))

        if self.mode == 'loop':
            self.points[self.idx + 1] = y
            self.idx = (self.idx + 2) % len(self.points)

        elif self.mode == 'scroll':
            self.points[3:self.num_pts*2:2] = self.points[1:self.num_pts*2-2:2]
            self.points[1] = y

        self.line.points = self.points

class MainWidget(BaseWidget) :
    def __init__(self):
        super(MainWidget, self).__init__()

        #clear log file
        open('log.txt', 'w').close()

        self.pitch = PitchDetector() #for graphics
        self.audio = Audio(NUM_CHANNELS, input_func=self.pitch.get_notes)
        self.mixer = Mixer()
        self.audio.set_generator(self.mixer)

        self.recording = False
        self.channel_select = 0
        self.input_buffers = []
        self.live_wave = None

        self.info = topleft_label()
        self.add_widget(self.info)

        self.anim_group = AnimGroup()

        # self.mic_meter = MeterDisplay((50, 25),  150, (-96, 0), (.1,.9,.3))
        # self.mic_graph = GraphDisplay((110, 25), 150, 300, (-96, 0), (.1,.9,.3))

        self.pitch_meter = MeterDisplay((50, 200), 150, (30, 90), (.9,.1,.3))
        self.pitch_graph = GraphDisplay((110, 200), 150, 300, (30, 90), (.9,.1,.3))

        # self.canvas.add(self.mic_meter)
        # self.canvas.add(self.mic_graph)
        self.canvas.add(self.pitch_meter)
        self.canvas.add(self.pitch_graph)

        self.canvas.add(self.anim_group)

        self.cur_pitch = 0
        self.status = 0

    def on_update(self) :
        self.audio.on_update()
        self.anim_group.on_update()

        # db = self.pitch.get_db()
        pitch = self.pitch.get_pitch()
        # self.mic_meter.set(db)
        # self.mic_graph.add_point(db)
        self.pitch_meter.set(pitch)
        self.pitch_graph.add_point(pitch)

        self.info.text = 'fps:%d\n' % kivyClock.get_fps()
        self.info.text += 'load:%.2f\n' % self.audio.get_cpu_load()
        self.info.text += 'volume:%.2f\n' % self.pitch.get_volume()
        self.info.text += "status: %s\n" % self.pitch.get_status()
        self.info.text += "pitch: %.1f\n" % pitch

        self.info.text += "r: toggle recording: %s\n" % ("OFF", "ON")[self.recording]

    def on_key_down(self, keycode, modifiers):
        # toggle recording
        if keycode[1] == 'r':
            if self.recording:
                self.pitch.stop_recording()
            else: self.pitch.start_recording("test.wav")
            self.recording = not self.recording

        # play back live buffer
        if keycode[1] == 'p':
            if self.live_wave:
                self.mixer.add(WaveGenerator(self.live_wave))

        if keycode[1] == 'c' and NUM_CHANNELS == 2:
            self.channel_select = 1 - self.channel_select

        if keycode[1] == 'g':
            print self.pitch.get_MIDI_data()

        if keycode[1] == 'a':
            self.pitch.write_MIDI_data()

        # adjust mixer gain
        gf = lookup(keycode[1], ('up', 'down'), (1.1, 1/1.1))
        if gf:
            new_gain = self.mixer.get_gain() * gf
            self.mixer.set_gain( new_gain )

run(MainWidget)