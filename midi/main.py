'''
main.py 

just a skeleton for now

'''

import sys
sys.path.append('..')

from common.core import *
from midi import *

from kivy.graphics.instructions import InstructionGroup
from kivy.graphics import Color, Ellipse, Rectangle, Line
from kivy.graphics import PushMatrix, PopMatrix, Translate, Scale, Rotate

from random import randint

NUM_CHANNELS = 2

class MainWidget(BaseWidget) :
    def __init__(self):
        super(MainWidget, self).__init__()
        self.midi = MidiInput(1)
        self.recording = False

    def on_key_down(self, keycode, modifiers):
        if keycode[1] == 'r':
            self.recording = not self.recording
            if self.recording: #stop recording
                self.midi.start_recording()
            else: #start recording
                self.midi.stop_recording()
            
        if keycode[1] == 'g':
            print str(self.midi.get_MIDI_data())

    def on_update(self):
        # print str(self.midi.get_message())
        self.midi.on_update()
        # print (str(self.midi.get_status()),str(self.midi.get_pitch()))

run(MainWidget)