'''
midi.py, Galen
for getting MIDI data from MIDI keyboard, sending to graphics (stream), sequencer (when asked)
only works with monophony right now
'''

import sys
sys.path.append('..')

import time

from rtmidi.midiutil import open_midiinput

from random import randint
import numpy as np

NUM_CHANNELS = 2

class MidiInput(object):
	def __init__(self, port):
		super(MidiInput,self).__init__()

		self.port = port
		self.time_start = time.time()
		self.time_cur = time.time()
		self.time_on = 0
		self.time_off = 0

		self.MIDI_data = [] #list of all events within a recording for sequencer

		self.last_on_times = np.zeros(32)
		self.statuses = ["holdoff"]*32
		self.cur_statuses = ["holdoff"]*32

		self.status = "off"
		self.pitch = 0
		self.velocity = 0
		self.cur_status = "off"

		self.noteon_time = 0
		self.duration = 0

		self.recording = False

		self.midiin = None
		self.open_port()

	def open_port(self):
		try:
			midiin, port_name = open_midiinput(self.port)
			self.midiin = midiin
		except (EOFError, KeyboardInterrupt):
			print "wrong port"

	def get_MIDI_data(self):
		return sorted(self.MIDI_data, key=lambda tup: tup[0])

	def start_recording(self, filename=None):
		self.recording = True
		self.MIDI_data = []
		self.time_start = time.time()

	def stop_recording(self):
		self.recording = False

	def get_pitch(self):
		return self.pitch

	def get_statuses(self):
		return self.cur_statuses

	def get_volume(self):
		return self.velocity

	#TODO: append MIDI data to self.MIDI_data for sequencer (need to to logic to figure out timings)
	#also need to holdon and holdoff for jason? (more logic)
	def on_update(self):
		message = self.midiin.get_message()

		if message: 
			self.pitch = message[0][1]
			if len(message[0]) > 3:
				self.velocity = message[0][2]

			if message[0][0] == 144: 

				#polyphony
				self.statuses[self.pitch-53] = "on"
				self.cur_statuses[self.pitch-53] = "on"

				# self.status = "on"
				# self.cur_status = "on"
				self.noteon_time = time.time()

				#polyphony try:
				self.last_on_times[self.pitch-53] = self.noteon_time

			elif message[0][0] == 128: 

				#polyphony
				self.statuses[self.pitch-53] = "off"
				self.cur_statuses[self.pitch-53] = "off"

				# self.status = "off"
				# self.cur_status = "off"
				# if self.recording and self.cur_status != "holdoff":
				# 	self.duration = time.time()-self.noteon_time
				# 	self.MIDI_data.append((self.noteon_time-self.time_start, self.duration, self.pitch, self.velocity))

				#polyphony
				if self.recording:
					self.duration = time.time()-self.last_on_times[self.pitch-53]
					self.MIDI_data.append((self.last_on_times[self.pitch-53]-self.time_start, self.duration, self.pitch, self.velocity))
				self.last_on_times[self.pitch-53] = 0

			else: 
				self.status = "controller message"
		else: 
			# print "self.status: " + str(self.status)
			#polyphony
			for i in range(len(self.statuses)):
				# print "statuses pitches: ", self.statuses[i] 
				if self.statuses[i] == "on": self.cur_statuses[i] = "holdon"
				else: 
					self.cur_statuses[i] = "holdoff"

			# if self.status == "on": 
			# 	self.cur_status = "holdon"
			# else: 
			# 	self.cur_status = "holdoff"
				# self.pitch = 0
				# self.velocity = 0
			# return (self.status, self.pitch)
