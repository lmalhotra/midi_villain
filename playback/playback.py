# -*- coding: utf-8 -*-

'''
playback.py, Laura

Requests Audio/MIDI from respective Modules when recording a track.
Interacts with its own stored data for tracks during playback. Gives Graphics Module data a few frames in advance to display falling notes
Inputs:
Data set from MIDI/Audio module
Outputs:
To Galen: Event when to start and stop recording
To Jason: Event when to begin displaying notes and loop barlines

TODO:
- make it possible to record into the next loop or choose to extend loop length
- why can't two synths play at same time with right duration?
- fix bug where it plays notes of track right after you play it if you stop recording before end of loop
- check timing of analog playback
- fix duration bug - right now recorded notes just last until the next note (or 0,0 if it's from my pad at the start and end)

'''
import sys
sys.path.append('..')

from common.core import *
from common.audio import *
from common.mixer import *
from common.wavegen import *
from common.wavesrc import *
from common.buffers import *
from common.synth import *
#from common.clock import *
import common.clock as c # really not sure why this is necessary...

from analog.analog import *
from graphics.graphics import set_falling_time

from random import random, randint
import numpy as np

MIN_LOOP_LENGTH = 8
FALLING_TIME = MIN_LOOP_LENGTH# how many seconds early to send note playback events to graphics

"""This class collects data from all recorded tracks, sends it to graphics module, and defines the synchronizing clock"""
class Sequencer(object):
    def __init__(self, mixer, pd, graphics, midi_input, sched):
        super(Sequencer, self).__init__()
        self.mixer = mixer
        self.pd = pd
        self.graphics = graphics
        self.midi_input = midi_input
        self.clock = sched.clock
        self.sched = sched
        self.channel = 1 # Initialize to 2 to get of Midi Input port's way??
        self.filename = None
        self.rec_start_time = None
        self.recording = False
        self.loop_length = MIN_LOOP_LENGTH # seconds (we can vary this later so user chooses the loop length)
        self.mode = 0 # midi mic input
        self.tracks = [] # elements of this list are Track objects (all hopefully take the same amount of time to play...)
        
        set_falling_time(FALLING_TIME)

        self.midi_input_synth = Synth("data/FluidR3_GM.sf2")
        self.synth_on = False
        self.on_note = 0
        self.mixer.add(self.midi_input_synth)

        self.set_instrument(0)
        if not self.clock.paused:
            self.graphics.add_line()
        self.barline_sent = False
        self.beats = 16
        self.beat_idx = 1
        self.started = True

    def play_toggle(self):
        if self.started:
            self.started = False
            self.graphics.set_play(False)
            self.clock.stop()
        else:
            self.started = True
            self.graphics.set_play(True)
            self.clock.start()

    def get_user_input(self,bpm,beats):
        self.loop_length = beats * (60*(1.0/bpm)) # seconds = beats * (minutes / beats ) * (seconds / minutes)
        #print "loop length = " + str(self.loop_length)
        FALLING_TIME = self.loop_length
        self.beats = beats
        set_falling_time(self.loop_length)
        self.started = True
        self.graphics.add_line(thick=True)

    def is_recording(self):
        return self.recording
    
    def loop_time_left(self):
        return self.loop_length - self.sched.get_time()

    def set_mode(self, modetype):
        self.mode = modetype

    def is_mode(self):
        return self.mode # 0: midi mic, 1: analog mic, 2: midi keyboard

    def mode_toggle(self,mode):
        if self.mode < 2:
            self.mode += 1
        else:
            self.mode = 0

    def midi_input_synth_on(self, pitch):
        # if not self.synth_on:
        self.synth_on = True
        self.on_note = pitch
        self.midi_input_synth.noteon(0, pitch, 115)

    def midi_input_synth_off(self, pitch):
        # if self.synth_on:
        self.synth_on = False
        self.midi_input_synth.noteoff(0, pitch)
        self.on_note = 0

    def set_instrument(self, idx):
        self.instrument = idx
        # if self.mode == 2:
        if self.instrument%5 == 0:
            self.midi_input_synth.program(0, 0, 2) # electric piano
        elif self.instrument%5 == 1:
            self.midi_input_synth.program(0, 0, 75) # pan flute
        elif self.instrument%5 == 2:
            self.midi_input_synth.program(0, 0, 71) # clarinet
        elif self.instrument%5 == 3:
            self.midi_input_synth.program(0, 0, 12) # marimba
        else:
            self.midi_input_synth.program(0, 128, 25) # TR

    # called by main.py when user starts recording; calls analog's start_recording
    def start_recording(self):
        if not self.recording and self.started:
            self.graphics.recording = True
            self.graphics.piano.base.set_color(True)
            self.rec_start_time = self.sched.get_time()
            self.filename = 'track' + str(self.channel) + '.wav'
            #print 'started track ' + str(self.channel)
            self.recording = True

            if self.mode == 2:
                self.midi_input.start_recording()
            else:
                self.pd.start_recording(self.filename)

        
    # called by main.py when user stops recording (or by analog module when it stops recording?); calls analog's get_midi_data() for the track just recorded
    def stop_recording(self):
        if self.recording:
            self.graphics.recording = False
            self.graphics.piano.base.set_color(False)
            self.recording = False
            #print 'stopped track' + str(self.channel)
            # track_length = self.sched.get_time() - self.rec_start_time
            # # if track_length > self.loop_length:
            # #     self.loop_length = track_length
            if self.mode == 2:
                self.midi_input.stop_recording()
                tuples = self.midi_input.get_MIDI_data()
                #print "KEYBOARD DATA tuples: "
                #print tuples
                # print "KEYBOARD DATA tuples: "
                # print tuples
            else:
                self.pd.stop_recording()
                tuples = self.pd.get_MIDI_data() # (time, duration, pitch, volume)
            
            unique_pitches = {}
            note_array = [] #list of sublists where #of sublists is # of unique MIDI keyboard notes in that track
            for tup in tuples:
                if tup[2] not in unique_pitches:
                    unique_pitches[tup[2]] = [tup]
                else: 
                    unique_pitches[tup[2]].append(tup)
            for key in unique_pitches:
                note_array.append(unique_pitches[key])
            #print note_array

            notes = []
            for i in xrange(len(tuples)):
                # % self.loop_length
                notes.append( ( (tuples[i][0] + self.rec_start_time), tuples[i][1], tuples[i][2], tuples[i][3] ) ) # not sure about adding rec_start_time...
                if i + 1 < len(tuples):
                    lag = tuples[i+1][0] - (tuples[i][0]+tuples[i][1])
                    if lag != 0: # if current note doesn't last all the way to next note, put in zeros
                        # print "lag: " + str(lag)
                        notes.append( (tuples[i][0]+tuples[i][1]+self.rec_start_time, lag, 0, 0) )
            if notes != []:
                # pad beginning and end of recording with zero so all tracks take same loop time
                start_pad = []
                end_pad = []
                if not self.rec_start_time <= 0:
                    start_pad = [(0, tuples[0][0]+self.rec_start_time, 0, 0)]
                if not notes[-1][0] + notes[-1][1] >= self.loop_length:
                    end_pad = [(tuples[-1][0]+self.rec_start_time+tuples[-1][1], self.loop_length -(tuples[-1][0]+self.rec_start_time+tuples[-1][1]) , 0, 0)] # last note's time + duration
                data = start_pad + notes + end_pad
                # print "data"
                # print data
            # print note_array

            if note_array != []:
                for n in xrange(len(note_array)): # for each of the unique pitch columns
                    tuples = note_array[n]
                    notes = []
                    for i in xrange(len(tuples)):
                        # % self.loop_length
                        notes.append( ( (tuples[i][0] + self.rec_start_time), tuples[i][1], tuples[i][2], tuples[i][3] ) ) # not sure about adding rec_start_time...
                        if i + 1 < len(tuples):
                            lag = tuples[i+1][0] - (tuples[i][0]+tuples[i][1])
                            if lag != 0: # if current note doesn't last all the way to next note, put in zeros
                                # print "lag: " + str(lag)
                                notes.append( (tuples[i][0]+tuples[i][1]+self.rec_start_time, lag, 0, 0) )
                
                    # pad beginning and end of recording with zero so all tracks take same loop time
                    start_pad = []
                    end_pad = []
                    if not self.rec_start_time <= 0:
                        start_pad = [(0, tuples[0][0]+self.rec_start_time, 0, 0)]
                    if not notes[-1][0] + notes[-1][1] >= self.loop_length:
                        end_pad = [(tuples[-1][0]+self.rec_start_time+tuples[-1][1], self.loop_length -(tuples[-1][0]+self.rec_start_time+tuples[-1][1]) , 0, 0)] # last note's time + duration
                    data = start_pad + notes + end_pad
                    note_array[n] = data ###

                if self.mode == 1: # analog mic input
                    wave_gen = WaveGenerator(WaveFile(self.filename),loop=False)
                else:
                    wave_gen = None

                wait = self.sched.get_time() < self.loop_length
                track = Track(note_array, self.sched, self.channel, self.mixer, self.graphics, wait, self.instrument, wave_gen, self.rec_start_time)
                self.channel += 1
                self.tracks.append(track)

    def undo(self):
        if len(self.tracks) > 0:
            track = self.tracks[-1] #track to remove
            for pitch in track.get_unique_pitches():
                track._midi_note_off(pitch)
            if not track.midi:
                track.wave_gen.reset()
            remove_trackid = track.channel
            self.tracks.remove(track)
            self.graphics.remove_falling_notes(remove_trackid)

    def on_update(self):
        if self.started:
            self.sched.on_update()
            now = self.sched.get_time()

            if self.mode == 2:
                statuses = self.midi_input.get_statuses()
                # print "statuses: ",statuses
                for i in range(len(statuses)):
                    if statuses[i] == "on":
                        self.midi_input_synth_on(i+53)
                    elif statuses[i] == "off":
                        self.midi_input_synth_off(i+53)

            elif self.synth_on: # just in case you're holding a note and then switch modes(?)
                for i in range(len(statuses)):
                    self.midi_input_synth_off(i)

            if now >= self.loop_length:
                self.graphics.add_line(thick=True)
                if self.recording:
                    self.stop_recording() # TODO take this out
                self.clock.set_time(0)
                now = 0
                self.beat_idx = 1
                for track in self.tracks:
                    track.on_loop_reset()

            if now >= (self.loop_length / self.beats)*self.beat_idx:
                self.graphics.add_line()
                self.beat_idx += 1
     
            for track in self.tracks:
                track.on_update(now, self.loop_length)

        self.graphics.set_mode(self.mode)


        
"""This class controls the playback of a single track"""
class Track(object):
    def __init__(self, data, sched, channel, mixer, graphics, wait, inst=0, wave_gen=None, wav_start_time=0):
        super(Track, self).__init__()
        self.data = data   # LIST of sublists where sublists contain tuples of midi (start_time, dur, pitch, volume)
        self.sched = sched
        self.channel = channel
        self.mixer = mixer
        self.wave_gen = wave_gen
        self.wav_start_time = wav_start_time
        self.inst = inst
        self.synth = Synth("data/FluidR3_GM.sf2")
        self.set_instrument(inst)
        if self.wave_gen == None:
            self.midi = True
            self.mixer.add(self.synth)
        else:
            self.midi = False
            self.mixer.add(self.wave_gen)

        self.graphics = graphics

        g = random()
        b = random()
        r = random()
        while (r + g + b < 1) or (r > 1.8*g and r > 1.5*b) or (g > 1.5*r and g > 1.5*b):
            c = np.random.randint(3)
            if c == 0: r = random()
            if c == 1: g = random()
            if c == 2: b = random()

        self.color = (r,g,b,0.8)

        self.on_note = 0
        self.wav_played = False
        self.times = []
        self.unique_pitches = set()
        for i in xrange(len(self.data)):
            self.times.append([])
            for j in xrange(len(self.data[i])): # self.data[i][j] is a tuple of (start_time, duration, pitch, volume)
                self.times[i].append(self.data[i][j][0])

                #get unique pitches
                self.unique_pitches.add(self.data[i][j][2])
                # print "unique_pitches: " + str(list(self.unique_pitches))

        # print self.times
        self.audio_idx = np.zeros(len(self.data),dtype=np.int) # need a different audio index for each unique pitch...
        self.prev_pitch = np.zeros(len(self.data),dtype=np.int)
        # print self.audio_idx
        self.sent = []
        self.on_note_graphic = None
        self.loop_count = 0
        self.wait = wait

    def get_unique_pitches(self):
        return list(self.unique_pitches)
        
    def set_instrument(self, inst=0):
        if inst%5 == 0:
            self.synth.program(self.channel, 0, 2) # electric piano
        elif inst%5 == 1:
            self.synth.program(self.channel, 0, 75) # pan flute
        elif inst%5 == 2:
            self.synth.program(self.channel, 0, 71) # clarinet
        elif inst%5 == 3:
            self.synth.program(self.channel, 0, 12) # marimba
        else:
            self.synth.program(self.channel, 128, 25) # TR

    def is_midi(self):
        return self.midi
        
    def _play_wav(self):
        self.wav_played = True
        self.wave_gen.play()
        # print "PLAYING WAVE FOR TRACK " + str(self.channel)
    
    def _midi_note_on(self, pitch, vel):
        if pitch: # pitch 0 is a rest
            if vel < 60:
                vel = 60
            elif vel > 120:
                vel = 120
            self.synth.noteon(self.channel, pitch, int(vel))
            # print 'playing audio note on track ' + str(self.channel) + ' at time ' + str(self.sched.get_time())

    def _midi_note_off(self, pitch):
        # terminate current note:
        # if self.on_note:
        if pitch:
            self.synth.noteoff(self.channel, pitch)
            # print "audio note off"
            # self.on_note = 0
            
    # when the loop ends, reset the wavegenerator and midi notes
    # called by Sequencer class
    def on_loop_reset(self):
        if not self.midi:
            self.wav_played = False
            self.wave_gen.reset()
            self.mixer.add(self.wave_gen)
            # print "RESETTING WAVE FILE FOR TRACK " + str(self.channel)
        self.sent = []
        self.audio_idx = np.zeros(len(self.data),dtype=np.int)
        self.loop_count += 1
            
        
    # called by Sequencer
    def on_update(self, now, loop_length):       
        if (not self.wait) or self.loop_count >= 1:
            for i in xrange(len(self.data)): # for each unique pitch
                # print "self.data[i]: "
                # print self.data[i]
                # print "audio idx[i] " + str(self.audio_idx[i])
                # play MIDI notes
                if self.audio_idx[i] < len(self.data[i]):
                    if now >= self.times[i][self.audio_idx[i]]:
                        # print 'play note in audio! ' + str(now)
                        _, dur, pitch, volume = self.data[i][self.audio_idx[i]]
                        if self.midi:
                            self._midi_note_off(self.prev_pitch[i]) # turn off old note
                            self._midi_note_on(pitch, volume) # play new note
                            self.prev_pitch[i] = pitch
                        if pitch:
                            self.graphics.add_falling_note(pitch, dur, self.color, self.channel)
                        self.audio_idx[i] += 1 # wait till time for next note
                        
            # play wav file if needed
            if not self.midi:
                if now >= self.wav_start_time and not self.wav_played:
                    self._play_wav()



        
        
