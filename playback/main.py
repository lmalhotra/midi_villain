'''
main.py 

to test playback.py

'''

import sys
sys.path.append('..')

from common.core import *
from common.audio import *
from common.writer import *
from common.mixer import *
from common.gfxutil import *
from common.wavegen import *
from common.buffers import *
from common.clock import *
from analog.analog import *
from midi.midi import *
from playback import *

from kivy.graphics.instructions import InstructionGroup
from kivy.graphics import Color, Ellipse, Rectangle, Line
from kivy.graphics import PushMatrix, PopMatrix, Translate, Scale, Rotate

from random import randint

NUM_CHANNELS = 2

class MainWidget(BaseWidget) :
    def __init__(self):
        super(MainWidget, self).__init__()
        self.pitch = PitchDetector()
        self.audio = Audio(NUM_CHANNELS, input_func=self.pitch.get_notes)
        self.mixer = Mixer()
        self.audio.set_generator(self.mixer)

        #potential objects?
        #self.graphics = Graphics() #jason's basewidget class
        self.sequencer = Sequencer(self.mixer, self.pitch) #laura's sequencer class

        self.recording = False

        self.info = topleft_label()
        self.add_widget(self.info)

    def on_key_down(self, keycode, modifiers):
        	# toggle recording
        if keycode[1] == 'r':
            if self.recording: #stop recording
                self.sequencer.stop_recording()
            else: #start recording
                self.sequencer.start_recording()
            self.recording = not self.recording
            
        if keycode[1] == 'm':
            self.sequencer.midi_analog_toggle()
            
        if keycode[1] == 'p':
            self.sequencer.toggle_playback()

        inst = lookup(keycode[1], '12345', (0,1,2,3,4))
        if inst != None:  
            self.sequencer.set_instrument(inst)

    def on_update(self) :
        self.audio.on_update()
        self.recording = self.sequencer.is_recording()
        self.sequencer.on_update()

        self.info.text = 'load: %.2f\n' % self.audio.get_cpu_load()
        self.info.text += 'gain: %.2f\n\n' % self.mixer.get_gain()
        self.info.text += 'loop time left: %.0f\n' % self.sequencer.loop_time_left()
        self.info.text += 'number of tracks: ' + str(len(self.sequencer.tracks)) + '\n'

        self.info.text += "r: toggle recording: %s\n" % ("OFF", "ON")[self.recording]
        self.info.text += "p: toggle playback: %s\n" % ("OFF","ON")[self.sequencer.is_playing()]
        self.info.text += "m: switch mode: %s\n" % ("analog","MIDI")[self.sequencer.is_midi_mode()]
        self.info.text += "1,2,3,4,5: switch instrument: %s\n" % ("electric piano","oboe","accordion","jazz guitar","marimba")[self.sequencer.instrument]


run(MainWidget)
